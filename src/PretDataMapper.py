import psycopg2
import string
from DataMapper import *

class PretDataMapper(DataMapper):

    def creerPret(self, login, ressources : list, duree) :
        # vérifier que l'adhérent ne soit pas suspendu 
        # l'adhérent n'a pas accès au système s'il est blacklisté
        connection = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = connection.cursor()
            cursor.execute("SELECT * FROM sanction s WHERE s.adherent = '{0}'").format(login)
            row = cursor.fetchone()
            if row != None:
                print('L\'adhérent est suspendu')

            sql1 = "INSERT INTO Pret(date, duree, adherent) VALUES (CURRENT_TIMESTAMP, '{1}', '{2}') RETURNING id".format(duree, login)
            cursor.execute(sql1)
            row = cursor.fetchone()
            pretID = row[0]

            for codeUnique in ressources :          
                cursor.execute("SELECT e.ref FROM Exemplaire e WHERE e.ressource = '{0}' and e.disponible = true and (e.etat = 'Neuf' OR e.etat = 'Bon')").format(codeUnique)
                row = cursor.fetchone()
                if row != None:
                    print('La ressource ne peut pas être empruntée')
                else :
                    ref = row[0]
                    sql2 = "INSERT INTO exemplairepret(pret, ref) VALUES ({0}, '{1}')".format(pretID, ref)
                    cursor.execute(sql2)
                    cursor.execute("UPDATE Exemplaire SET disponible = false WHERE ref = '{0}'".format(ref))

            connection.commit()
        except Exception as err:
            print(err)
        finally:
            connection.close()

    def retourPret(self, pretID) : 
        connection = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = connection.cursor()
            cursor.execute("SELECT ep.ref FROM ExemplairePret ep WHERE ep.pret = {0}".format(pretID))
            rows = cursor.fetchall()
            for row in rows : 
                ref = row[0]
                cursor.execute("UPDATE Exemplaire SET disponible = true WHERE ref = '{0}'".format(ref))
            connection.commit()
        except Exception as err:
            print(err)
        finally:
            connection.close()

    def getExemplairesPret(self, pretID) : 
        connection = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = connection.cursor()
            cursor.execute("SELECT * FROM ExemplairePret ep WHERE ep.pret = {0}".format(pretID))
            rows = cursor.fetchall()
            return rows
        except Exception as err:
            print(err)
        finally:
            connection.close()
    
    def miseAJourEtatExemplaire(self, ref, etat) : 
        connection = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = connection.cursor()
            cursor.execute("UPDATE Exemplaire SET etat = '{0}' WHERE ref = '{1}'".format(etat, ref)) 
            connection.commit()          
        except Exception as err:
            print(err)
        finally:
            connection.close()
 

