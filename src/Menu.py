from typing import Tuple

from AdherentDataMapper import *
from UserDataMapper import UserDataMapper, Roles
from StatistiquesDataMapper import StatistiquesDataMapper
from SanctionDataMapper import SanctionDataMapper
from RessourceDataMapper import RessourceDataMapper
from PretDataMapper import PretDataMapper
import datetime
def UserConnection():
    #permet à l'utilisateur de saisir ces identifiants de connexion
    print("---------Bonjour, bienvenue à la bibliothèque----------")
    login = input("Entrer votre login:")
    mdp = input("Entrer votre mot de passe:")
    return (login,mdp)

def affiche(listTuple:list, headers:Tuple):
    print("====================================================================================================================================")
    for header in headers:
        print("{:>30s}".format(header), end="")
    print("")
    print("====================================================================================================================================")
    if listTuple:
        for row in listTuple:
            for data in row:
                print("{:>30s}".format(str(data)),end="")
            print(" ")
def addAdherent():
    print("--------------Ajout d'un adhérent--------------------------")
    login = input("Entrer un nouveau login : ")
    numcarte = input("Entrer le numéro de carte : ")
    mdp = input("Entrer un nouveau mot de passe : ")
    nom = input("Entrer votre nom : ")
    prenom = input("Entrer votre prenom : ")
    adresse = input("Entrer votre adresse : ")
    adressemail = input("Entrer votre adresse mail : ")
    tel = input("Entrer votre numéro de téléphone : ")
    dateNaissance = datetime.strptime(input("Entrer votre date de naissance : "))
    return(login,numcarte,mdp,nom,prenom,adresse,adressemail,tel,dateNaissance)

def addPersonnel():
    print("--------------Ajout d'un adhérent--------------------------")
    login = input("Entrer un nouveau login : ")
    mdp = input("Entrer un nouveau mot de passe : ")
    nom = input("Entrer votre nom : ")
    prenom = input("Entrer votre prenom : ")
    adresse = input("Entrer votre adresse : ")
    adressemail = input("Entrer votre adresse mail : ")
    return(login,mdp,nom,prenom,adresse,adressemail)


def actionAdherent():
    print("------------------------MENU--------------------------------")
    print("1 : Voir les exemplaires disponibles")
    print("2 : Voir les exemplaires que vous avez empruntés")
    print("-1 : Se déconnecter de votre compte.")
    action = -2
    while action == -2:
        try:
            action = int(input("Veuillez saisir le numéro de votre action : "))
        except Exception:
            action = -2
    return(action)


def actionMember():
    print("------------------------MENU--------------------------------")
    print("1 : Gérer les emprunts.")
    print("2 : Gérer les sanctions.")
    print("3 : Gérer les ressources.")
    print("4 : Voir les statistiques de la bibliothèque." )
    print("5 : Gérer les utilisateurs.")
    print("-1 : Se déconnecter de son compte.")
    action = -2
    while action == -2:
        try:
            action = int(input("Veuillez saisir le numéro de votre action : "))
        except Exception:
            action = -2
    return(action)



def sanction():
    print("---------------------Gérer les sanctions---------------------------")
    print("1 : Blacklister un adhérent.")
    print("2 : Historique des sanctions.")
    action = -2
    while action == -2:
        try:
            action = int(input("Veuillez saisir le numéro de votre action : "))
        except Exception:
            action = -2
    return(action)

def userManager():
    print("---------------------Gérer les utilisateurs---------------------------")
    print("1 : Ajouter un adhérent.")
    print("2 : Ajouter un membre du personnel.")
    print("3 : Supprimer un adhérent.")
    print("4 : Supprimer un membre du personnel.")
    action = -2
    while action == -2:
        try:
            action = int(input("Veuillez saisir le numéro de votre action : "))
        except Exception:
            action = -2
    return(action)

def loaning():
    print("---------------------Gérer les ressources---------------------------")
    print("1 : Voir les ressources disponibles.")
    print("2 : Voir la répartition des emprunts.")
    print("3 : Ajouter un livre.")
    print("4 : Ajouter une oeuvre musicale.")
    print("5 : Ajouter un film.")
    action = -2
    while action == -2:
        try:
            action = int(input("Veuillez saisir le numéro de votre action : "))
        except Exception:
            action = -2
    return(action)



def Statistiques():
    print("---------------------Voir les statistiques-------------------------")
    print("1 : Voir les ressources populaires.")
    print("2 : Voir le nombre d'emprunts par ressource.")
    print("3 : Voir les genres populaires.")
    print("4 : Voir le nombre d'emprunts par ressource.")
    print("5 : Voir le genre préféré des adhérents.")
    print("6 : Voir l'âge moyen des adhérents'.")
    print("7 : Voir les types de documents populaires.")
    print("8 : Voir la durée moyenne d'un emprunt par adhérent.")
    print("9 : Voir la durée moyenne d'un emprunt par adhérent.")
    print("10 : Voir la durée max d'un emprunt par adhérent.")
    print("11 : Voir la durée max des prêts.")
    print("12 : Voir la durée moyenne des emprunts par ressource.")
    print("13 : Voir la durée max d'un emprunt par ressource.")
    action = -2
    while action == -2:
        try:
            action = int(input("Veuillez saisir le numéro de votre action : "))
        except Exception:
            action = -2
    return(action)


def addLivre():
    codeunique = input("Entrer le code du livre : ")
    titre = input("Entrer le titre du livre : ")
    dateapparition = datetime.strptime(input("Entrer la date du livre : "))
    editeur = input("Entrer l'éditeur du livre : ")
    genre = input("Entrer le genre du livre : ")
    codeclassification = input("Entrer le code de classification du livre : ")
    isbn = input("Entrer le code ISBN du livre : ")
    resume = input("Entrer le résumé du livre : ")
    langue = input("Entrer la langue du livre : ")
    return (codeunique, titre, dateapparition, editeur, genre, codeclassification, isbn, resume, langue)

def addOeuvremusical():
    codeunique = input("Entrer le code de l'oeuvre musicale : ")
    titre = input("Entrer le titre de l'oeuvre musicale : ")
    dateapparition = datetime.strptime(input("Entrer la date de l'oeuvre musicale : "))
    editeur = input("Entrer l'éditeur de l'oeuvre musicale : ")
    genre = input("Entrer le genre de l'oeuvre musicale : ")
    codeclassification = input("Entrer le code de classification de l'oeuvre musicale : ")
    longueur = input("Entrer la longueur de l'oeuvre musicale : ")
    return (codeunique, titre, dateapparition, editeur, genre, codeclassification, longueur)

def addFilm():
    codeunique = input("Entrer le code du film : ")
    titre = input("Entrer le titre du film : ")
    dateapparition = datetime.strptime(input("Entrer la date du film : "))
    editeur = input("Entrer l'éditeur du film : ")
    genre = input("Entrer le genre du film : ")
    codeclassification = input("Entrer le code de classification du film : ")
    longueur = input("Entrer la longueur du film : ")
    synopsis = input("Entrer le synopsis du film : ")
    return (codeunique, titre, dateapparition, editeur, genre, codeclassification, longueur, synopsis)


def pretmanager():
    print("---------------------Gérer les prêts---------------------------")
    print("1 : Faire un emprunt.")
    print("2 : Faire un retour.")
    print("3 : Voir la liste des exemplaires empruntés.")
    print("4 : Mettre à jour l'état d'un exemplaire.")
    action = int(input("Veuillez saisir le numéro de votre action : "))
    return (action)

def Menu():
    while(1):
        login, mdp = UserConnection()
        print(login , mdp)
        dataMapper = UserDataMapper()
        role = dataMapper.checkUser(login,mdp)

        if (role == Roles.PERSONNEL):
            action = 0
            while(action!=-1):
                action = actionMember()
                if(action==1):
                    dataMapper= PretDataMapper()
                    act =  pretmanager()
                    if(act==1):
                        login = input("Entrez le login de l'adhérent : ")
                        ressources_str = input("Entrez les ids d'exemplaires séparés par une virgule (ex : 7,8,9,10) :")
                        ressources = ressources_str.split(",")
                        duree = input("Entrez le nombre de jours pour le prêt : ")
                        dataMapper.creerPret(login, ressources, duree)
                    elif(act==2):
                        pseudo = input("Entrez le login de l'adhérent : ")
                        data = dataMapper.getPrets(pseudo)
                        affiche(data, ('NbArticle', 'Id', 'Date', 'login'))
                        pretID = input("Entrez le numéro de prêt : ")
                        dataMapper.retourPret(pretID)
                    elif(act==3):
                        pretID = input("Entrez le numéro du prêt : ")
                        data = dataMapper.getExemplairesPret(pretID)
                        affiche(data, ("Réf.", "N° Pret"))
                    elif(act==4):
                        ref = input("Entrez la référence de l'exemplaire : ")
                        etat = input("Saisissez l'état de l'exemplaire ('Abimé', 'Perdu', 'Bon', 'Neuf') :")
                        dataMapper.miseAJourEtatExemplaire(ref, etat)

                elif(action==2):
                    dataMapper = SanctionDataMapper()
                    act = sanction()
                    if (act == 1):
                        login = input("Saisir le login de l'adhérent à ajouter à la blacklist : ")
                        dataMapper.blacklister(login)
                    elif (act == 2):
                        data = dataMapper.getSanctions()
                        affiche(data, ("Id", "Adhérent", "Date de la sanction", "En cours"))
                elif (action == 3):
                    dataMapper = RessourceDataMapper()
                    act = loaning()
                    if (act == 1):
                        data = dataMapper.getAvailableExemplaires()
                        affiche(data, ("Nb articles", "Titre", "Editeur", "Genre"))
                    elif (act == 2):
                        data = dataMapper.getRepartitionEmprunts()
                        affiche(data, ("Document", "Nb d'emprunts", "Part d'emprunts"))
                    elif (act == 3):
                        codeunique, titre, dateapparition, editeur, genre, codeclassification, isbn, resume, langue = addLivre()
                        dataMapper.ajouterRessource(codeunique, titre, dateapparition, editeur, genre,codeclassification)
                        dataMapper.ajouterLivre(codeunique, titre, dateapparition, editeur, genre,codeclassification, isbn, resume, langue)


                    elif (act == 4):
                        codeunique, titre, dateapparition, editeur, genre, codeclassification, longueur = addOeuvremusical()
                        dataMapper.ajouterRessource(codeunique, titre, dateapparition, editeur, genre,codeclassification)
                        dataMapper.ajouterOeuvreMusicale(codeunique, titre, dateapparition, editeur, genre,codeclassification, longueur)
                    elif (act == 5):
                        codeunique, titre, dateapparition, editeur, genre, codeclassification, longueur, synopsis = addFilm()
                        dataMapper.ajouterRessource(codeunique, titre, dateapparition, editeur, genre,codeclassification)
                        dataMapper.ajouterFilm(codeunique, titre, dateapparition, editeur, genre,codeclassification, longueur, synopsis)
                elif (action == 4):
                    dataMapper=StatistiquesDataMapper()
                    act = Statistiques()
                    if(act==1):
                        data=dataMapper.ressourcesPopulaires()
                        affiche(data, ("Ressource", "Nb emprunts"))
                    elif(act==2):
                        codeunique = input("Entrez la référence de la ressource : ")
                        data=dataMapper.nombreEmpruntRessource(codeunique)
                        print(f"La ressource a été empruntée {data} fois")
                    elif (act == 3):
                        data=dataMapper.genresPopulaires()
                        affiche(data, ("Genre", "Nb emprunts"))
                    elif (act == 4):
                        genre = input("Entrer :")
                        data=dataMapper.nombreEmpruntParGenre(genre)
                        print(f"Le nombre d'emprunts pour le genre {genre} est de {data}.")
                    elif (act == 5):
                        login = input("Entrer le login d'un adhérent:")
                        data=dataMapper.genrePrefereAdherent(login)
                        print(f"Le genre préféré de l'adhérent {login} est '{data}'.")
                    elif (act == 6):
                        data=dataMapper.ageMoyenAdherent()
                        print(f"L'âge moyen des adhérents est de {data}.")
                    elif (act == 7):
                        data=dataMapper.typeDocumentPopulaire()
                        print(f"Les documents les plus empruntés sont les {data}.")
                    elif (act == 8):
                        login = input("Entrer le login d'un adhérent:")
                        data=dataMapper.dureeMoyenneEmpruntParAdherent(login)
                        print(f"La durée moyenne d'un emprunt par l'adhérent {login} est de {data} jours.")
                    elif (act == 9):
                        data=dataMapper.dureeMoyenneEmprunt()
                        print(f"La durée moyenne d'un emprunt est de {data} jours.")
                    elif (act == 10):
                        login = input("Entrer le login d'un adhérent:")
                        data=dataMapper.dureeMaxEmpruntParAdherent(login)
                        print(f"Le plus long emprunt de {login} a duré {data} jours.")
                    elif (act == 11):
                        data=dataMapper.dureeMaxPret()
                        print(f"Le plus long emprunt a duré {data} jours.")
                    elif (act == 12):
                        codeunique = input("Entrer le code de la ressource : ")
                        data=dataMapper.dureeMoyenneEmpruntParRessource(codeunique)
                        print(f"Cette ressource a été empruntée {data} jours en moyenne.")
                    elif (act == 13):
                        codeunique = input("Entrer le code de la ressource : ")
                        data=dataMapper.dureeMaxEmpruntParRessource(codeunique)
                        print(f"Cette ressource a été empruntée au maximum {data} jours.")

                elif (action == 5):
                    dataMapper = UserDataMapper()
                    act = userManager()
                    if(act==1):
                        login, numcarte, modtdepasse, nom, prenom, adresse, mail, numtel, datenaissance = addAdherent()
                        dataMapper.ajouterUtilisateur(login, modtdepasse, nom, prenom, adresse, mail)
                        dataMapper.ajouterAdherent(login, numcarte, datenaissance, numtel)
                    elif(act==2):
                        login, modtdepasse, nom, prenom, adresse, mail = addPersonnel()
                        dataMapper.ajouterUtilisateur(login, modtdepasse, nom, prenom, adresse, mail)
                        dataMapper.ajouterPersonnel(login)
                    elif (act == 3):
                        login = input("Entrer le login de l'adhérent à supprimer : ")
                        dataMapper.deleteAdherent(login)
                    elif (act == 4):
                        login = input("Entrer le login du membre du personnel à supprimer : ")
                        dataMapper.deletePersonnel(login)
                    elif (act == 5):
                        adherentDataMapper = AdherentDataMapper()
                        data = adherentDataMapper.selectAdherents()
                        affiche(data, ("Login", "N° carte", "Date de naissance", "Tél.", "Date d'adhésion", "Blacklisté"))
        elif(role==Roles.ADHERENT):
            action=0
            while(action!=-1):
                dataMapper = RessourceDataMapper()
                action=actionAdherent()
                if (action == 1):
                    data=dataMapper.getAvailableExemplaires()
                    affiche(data, ('Réf.','Titre','Editeur','Genre'))
                elif (action == 2):
                    dataMapper= AdherentDataMapper()
                    data=dataMapper.getPrets(login)
                    affiche(data, ('NbArticles','Id','Date','login'))
        elif(role==Roles.UNAUTHORIZED):
            print("Vous n'êtes pas enregistré à la bibliothèque.")
        elif(role==Roles.BLACKLISTED):
            print("Vous faites partie de la liste noire de la bibliothèque.\nVous ne pouvez donc plus faire d'emprunts.")
Menu()