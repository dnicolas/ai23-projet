import psycopg2
import psycopg2.extensions


class DataMapper:
    # Chaîne de connexion
    host = "localhost"
    user = "postgres"
    password = "root"
    database = "ai23-projet"

    def connect(self) -> psycopg2.extensions.connection:
        res = None
        try:
            res = psycopg2.connect(
            host=self.host,
            database=self.database,
            user=self.user,
            password=self.password)
        except psycopg2.OperationalError:
            print("Database connection error ")
        return res