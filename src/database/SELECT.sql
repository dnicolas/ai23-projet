
-- Retourne la liste des prêts de l'adhérent dnicolas

SELECT COUNT(exemplairepret.pret) AS nb_articles,pret.pretid, pret.date, pret.adherent
FROM pret JOIN exemplairepret ON pret.pretid = exemplairepret.pret "
WHERE pret.adherent = 'dnicolas' GROUP BY pret.pretid;

-- Retourne l'âge moyen des adhérents
SELECT AVG(DATE_PART('year', now()) - DATE_PART('year', a.dateNaissance)) FROM Adherent a;

-- Retourne les adhérents blacklistés
SELECT * FROM Adherent WHERE blacklisted = TRUE;

-- Retourne la liste des sanctions des adhérents
SELECT sanctionid, adherent, datesanction, encours FROM sanction;

-- Retourne la liste des membres du personnel
SELECT * FROM personnel;
