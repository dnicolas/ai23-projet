/* ---------------- UTILISATEUR TABLE DATASET ---------------- */

INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('hlefebvre', '1888ZFDSDSQD5Q', 'Lefebvre', 'Hugo', '78 Avenue des Champs-Elysées - 75000 Paris', 'hugo.lefebvre@gmail.com');
INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('dnicolas', '4868775SDFQS', 'Nicolas', 'Dimitri', '18 Rue Victor Hugo - 60000 Beauvais', 'dimitri.nicolas@etu.utc.fr');
INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('mmarsal', '687DSQSQD45FDQS', 'Marsal', 'Marine', '2 Avenue de la liberté - 75000 Paris', 'marine.marsal@etu.utc.fr');
INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('msolan', 'XDSFQS867SDQ687', 'Solan', 'Matthieu', '5bis rue du Général de Gaulle - 60100 Creil', 'matthieu.solan@etu.utc.fr');
INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('blussier', 'FQSD6SQD67QS', 'Lussier', 'Benjamin', '10 Avenue des martyrs de la liberté - 60200 Compiègne', 'benjamin.lussier@utc.fr');
INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('avictorino', '6876QSD76QSD687Q', 'Victorino', 'Alessandro', '67 rue roger couttolenc - 60200 Compiègne', 'alessandro.victorino@utc.fr');
INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('cdupont', 'DSQF6878DQ7', 'Dupont', 'Christophe', '2 Rue de Paris - 95000 Cergy', 'christophe.dupont@utc.fr');
INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('hbernard', 'SDFQS8787QBDQSAX', 'Bernard', 'Henri', '25 Avenue des Champs-Elysées - 75000 Paris', 'henri.bernard@orange.fr');
INSERT INTO utilisateur(
login, motdepasse, nom, prenom, adresse, mail)
VALUES ('lbernard', '47FEADQ665D788', 'Bernard', 'Louis', '25 Avenue des Champs-Elysées - 75000 Paris', 'louis.bernard@orange.fr');


/* ---------------- ADHERENT TABLE DATASET ---------------- */

INSERT INTO adherent(
	login, numcarte, datenaissance, numtel, dateadhesion, blacklisted)
	VALUES ('hlefebvre', '1255489656', '13/05/2000', '0344586921', CURRENT_TIMESTAMP, FALSE);
	INSERT INTO adherent(
	login, numcarte, datenaissance, numtel, dateadhesion, blacklisted)
	VALUES ('dnicolas', '5464544556', '12/04/2001', '0658697423',CURRENT_TIMESTAMP, FALSE);
	INSERT INTO adherent(
	login, numcarte, datenaissance, numtel, dateadhesion, blacklisted)
	VALUES ('mmarsal', '2568689864', '23/10/1999', '0345867796',CURRENT_TIMESTAMP, FALSE);
	INSERT INTO adherent(
	login, numcarte, datenaissance, numtel, dateadhesion, blacklisted)
	VALUES ('msolan', '34868452136', '29/12/1999', '0678221436',CURRENT_TIMESTAMP, FALSE);
	INSERT INTO adherent(
	login, numcarte, datenaissance, numtel, dateadhesion, blacklisted)
	VALUES ('cdupont', '78786578654', '20/05/1985', '0365789611',CURRENT_TIMESTAMP, FALSE);
	INSERT INTO adherent(
	login, numcarte, datenaissance, numtel, dateadhesion, blacklisted)
	VALUES ('hbernard', '135678654456', '02/08/2000', '0458961232',CURRENT_TIMESTAMP, TRUE);
	INSERT INTO adherent(
	login, numcarte, datenaissance, numtel, dateadhesion, blacklisted)
	VALUES ('lbernard', '12224044456', '06/03/1998', '0656321287',CURRENT_TIMESTAMP, TRUE);

/* ---------------- PERSONNEL TABLE DATASET ---------------- */
INSERT INTO personnel(
	login)
	VALUES ('blussier');
INSERT INTO personnel(
	login)
	VALUES ('avictorino');
INSERT INTO personnel(
	login)
	VALUES ('cdupont');

/* --------------- RESSOURCES DATASET IMPORTS --------------------------*/
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (12345, 'DocumentaireSurLeschats', '2021-06-10', 'Pedro', 'Documentaire Animalié', 45);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (4562, 'LaCuisineItaliene', '1950-10-10', 'Luigi', 'culinaire', 2);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (8921, 'LeVoyageDeChiiro', '2006-05-02', 'studio Guibli', 'animation', 8);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (2345, 'Lachanson', '2015-02-10', 'unediteurparticulié', 'musical', 1);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (7896, 'Mickey', '1950-05-10', 'WaltDisney', 'animation', 6);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (5325, 'les 3 mousquetaires', '1990-05-02', 'unécrivain', 'romain', 55);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (98756, 'machin', '1985-05-02', 'machin', 'truc', 81);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (8254, 'le song', '1598-02-10', 'le chanteur', 'musical', 87);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (4873, 'Tintin', '1950-05-10', 'Justin Bridou', 'Bande dessiné', 56);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (4123, 'One Piece', '1999-05-10', 'Toei Animation', 'Bande dessiné', 33);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (4942, 'programmation language C', '2015-02-18', 'Open Classroom', 'Informatique', 19);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (1713, 'Asterix et Obelix', '1985-10-02', 'Hachette', 'Bande dessiné', 37);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (7623, 'Gaston la gaffe', '1990-08-15', 'Hachette', 'Bande dessiné', 334);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES (9826, 'Le banquet de Platon', '0387-10-02', 'Folio', 'Philosophie', 999);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (1752, 'Le Complot Contre l''amérique', '10/03/1995', 'Hachette', 'Roman', 543);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (7453, 'Le songe d''une nuit d''été', '08/04/1750', 'Hachette', 'Théatre', 5169);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (5625, 'Avenger', '02/10/2018', 'Marvel', 'Super Héros', 734);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (437, 'Le roi Lion', '05/12/1985', 'Disney', 'animation', 4762);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (8791, 'Blanche neige et Les 7 nains', '01/09/1973', 'Disney', 'animation', 3489);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (3495, 'Avatar', '10/03/2008', 'Dolby', 'Science-Fiction', 54632);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (888, 'Aladin', '02/06/1996', 'Disney', 'Animation', 4632);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (3496, 'Star Wars', '02/10/1995', 'Lucas Film', 'Science-Fiction', 5462);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (6666, 'On m''appelle l''ovni', '16/12/2016', 'Jul', 'Rap', 5423);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (7862, 'Crazy in Love', '02/7/1975', 'Jay-Z', 'Classic', 4622);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (4365, 'Like a Rolling Stone', '14/05/1980', 'Bob Dylan', 'Rock', 4645);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (9133, 'Imagine', '10/05/1970', 'John Lennon', 'Rock', 7346);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (222, 'Satisfaction', '02/07/1987', 'The Rolling Stones', 'Rock', 5482);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (127, 'What''s Going on', '11/04/1995', 'Marving Gaye', 'pop', 797);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (731, 'Clocks', '05/4/2012', 'Coldplay', 'pop', 789);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (134, 'Respect', '02/10/2007', 'Aretha Franklin', 'Rock', 716);
INSERT INTO ressource(
	codeunique, titre, dateapparition, editeur, genre, codeclassification)
	VALUES (1111, 'Johny B. Goode', '12/07/1970', 'Hey Jude', 'Rap', 7232);

/*-------------------------------- OEUVRE MUSICALES DATASET IMPORTS ----------------------------------*/
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (6666, 5);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (4365, 4);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (9133, 6);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (222, 3);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (127, 5);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (731, 4);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (134, 6);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (1111, 5);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (7862, 4);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (8254, 3);
INSERT INTO oeuvremusicale(
	codeunique, longueur)
	VALUES (2345, 7);

/*----------------------------------- FILMS DATASET IMPORTS -------------------------------------------*/

INSERT INTO film(
	codeunique, longueur, synopsis) VALUES (12345, 2, 'la vie secréte des chats');
INSERT INTO film(
	codeunique, longueur, synopsis) VALUES (8921, 2, 'chiiro perdu');
INSERT INTO film(
	codeunique, longueur, synopsis)
	VALUES (5625, 3, 'des supers héroes défendent la terre');
INSERT INTO film(
	codeunique, longueur, synopsis)
	VALUES (437, 1.5, 'Simba devient roi de la jungle');
INSERT INTO film(
	codeunique, longueur, synopsis)
	VALUES (8791, 2, 'Blanche neige vient vivre avec des nains');
INSERT INTO film(
	codeunique, longueur, synopsis)
	VALUES (3495, 3, 'Decouverte d''une nouvelle planette habité');
INSERT INTO film(
	codeunique, longueur, synopsis)
	VALUES (888, 2, 'l''histoire d''un jeune voleur');
INSERT INTO film(
	codeunique, longueur, synopsis)
	VALUES (3496, 3, 'dans une galaxy lointaine');
INSERT INTO film(
	codeunique, longueur, synopsis)
	VALUES (7896, 3, 'Salut les amis, c''est moi Mickey, HOHO');
INSERT INTO film(
	codeunique, longueur, synopsis)
	VALUES (98756, 5, 'le film à voir une fois dans sa vie');

INSERT INTO livre(codeunique, isbn, resume, langue) VALUES (4562, 'ita52', 'recette de cuisine italienne','italien');
INSERT INTO livre(codeunique, isbn, resume, langue) VALUES (5325, 'ds52g', 'la vie des 3 mousquetaires', 'Français');
INSERT INTO livre(
	codeunique, isbn, resume, langue)
	VALUES (4873, 'zetg56', 'Tintin et Milou', 'Français');
INSERT INTO livre(
	codeunique, isbn, resume, langue)
	VALUES (4123, 'sd522dd', 'Pirate', 'Japonais');
INSERT INTO livre(
	codeunique, isbn, resume, langue)
	VALUES (4942, 'ze23dgs', 'Apprentissage C++', 'Français');
INSERT INTO livre(
	codeunique, isbn, resume, langue)
	VALUES (1713, '5q46ds21', 'un village gaulois résiste à la conquête romaine', 'Français');
INSERT INTO livre(
	codeunique, isbn, resume, langue)
	VALUES (7623, 'dqf6512dsg', 'la vie d''un jeune garçon flemmard et maladroit mais très malin', 'Français');
INSERT INTO livre(
	codeunique, isbn, resume, langue)
	VALUES (9826, 'f64fds2fd', 'débat sur le thême de l''amour durant un banquet', 'Grec');
INSERT INTO livre(
	codeunique, isbn, resume, langue)
	VALUES (7453, 'dsgdsg52','des jeunes gens se perdent durant la nuit dans une forêt','Français' );
INSERT INTO livre(
	codeunique, isbn, resume, langue)
	VALUES (1752, '6sd23ds1','la vie du petit philippe lorsque Lindberg devient président des états-unis','Anglais' );

/*-------------------------- TABLE EXEMPLAIRE IMPORTS ------------------------*/
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('5506', 'Bon', TRUE, '12345');
INSERT INTO exemplaire(
ref, etat, disponible, ressource)
VALUES ('5505', 'Neuf', TRUE, '12345');
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('5507', 'Bon', TRUE, '4562');
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
VALUES ('5508', 'Neuf', TRUE, '8921');
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('5509', 'Bon', TRUE, '7896');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
VALUES ('5510', 'Neuf', FALSE, '7896');
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('5696', 'Abimé', TRUE, '5325');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
VALUES ('6000', 'Neuf', FALSE, '4873');
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('8965', 'Bon', TRUE, '4123');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('8966', 'Bon', TRUE, '4123');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('8967', 'Neuf', TRUE, '4123');	
	INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('9000', 'Bon', TRUE, '4942');	
	INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('623', 'Neuf', FALSE, '1713');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('1125', 'Bon', TRUE, '7623');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('1156', 'Bon', TRUE, '9826');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('8896', 'Neuf', TRUE, '6666');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('10056', 'Bon', FALSE, '3496');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('10057', 'Bon', TRUE, '3496');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('12300', 'Bon', TRUE, '3495');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('7896', 'Neuf', TRUE, '888');	
INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('5236', 'Abimé', TRUE, '7453');	
	INSERT INTO exemplaire(
	ref, etat, disponible, ressource)
	VALUES ('11456', 'Abimé', TRUE, '5625');	
    /* --------------- SANCTION DATASET IMPORTS --------------- */
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (TRUE, CURRENT_TIMESTAMP, 'dnicolas');
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (FALSE, '10-05-2020', 'dnicolas');
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (TRUE, CURRENT_TIMESTAMP, 'msolan');
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (FALSE, '08/05/2020', 'hbernard');
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (FALSE, '10/05/2020', 'lbernard');
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (FALSE, '01/06/2020', 'hbernard');
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (FALSE, '01/06/2020', 'lbernard');
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (TRUE, CURRENT_TIMESTAMP, 'hbernard');
INSERT INTO sanction(encours, datesanction, adherent)
	VALUES (TRUE, CURRENT_TIMESTAMP, 'lbernard');
/* --------------- SUSPENSION DATASET IMPORTS --------------- */
INSERT INTO suspension(
	sanctionid, duree)
	VALUES (2, 7);
INSERT INTO suspension(
	sanctionid, duree)
	VALUES (1, 14);
INSERT INTO suspension(
	sanctionid, duree)
	VALUES (3, 7);
INSERT INTO suspension(
	sanctionid, duree)
	VALUES (4, 21);
INSERT INTO suspension(
	sanctionid, duree)
	VALUES (5, 7);

/* --------------- REMBOURSEMENT DATASET IMPORTS --------------- */
INSERT INTO remboursement(
	sanctionid, dateecheance, montant, refexemplaire)
VALUES (1, TO_DATE('30/06/2021', 'DD/MM/YYYY'), 15.0, '8965');
INSERT INTO remboursement(
	sanctionid, dateecheance, montant, refexemplaire)
	VALUES (4, TO_DATE('31/05/2020', 'DD/MM/YYYY'), 10.0, '5696');
INSERT INTO remboursement(
	sanctionid, dateecheance, montant, refexemplaire)
	VALUES (3, TO_DATE('30/06/2021', 'DD/MM/YYYY'), 20.0, '5236');
INSERT INTO remboursement(
	sanctionid, dateecheance, montant, refexemplaire)
	VALUES (6, TO_DATE('30/06/2020', 'DD/MM/YYYY'), 12.0, '11456');

/*-------------------- TABLE PRET DATASET IMPORTS ----------------------*/
INSERT INTO pret(date, duree, adherent)
	VALUES ('08-06-2020', 15, 'cdupont');
INSERT INTO pret(date, duree, adherent)
	VALUES ('10-06-2020', 21, 'dnicolas');
INSERT INTO pret(date, duree, adherent)
	VALUES ('12-06-2020', 15, 'dnicolas');
INSERT INTO pret(date, duree, adherent)
	VALUES (CURRENT_TIMESTAMP, 10, 'mmarsal');
INSERT INTO pret(date, duree, adherent)
	VALUES (CURRENT_TIMESTAMP, 10, 'msolan');
INSERT INTO pret(date, duree, adherent)
	VALUES ('12-06-2021', 15, 'hlefebvre');
INSERT INTO pret(date, duree, adherent)
	VALUES ('10-09-2020', 15, 'hbernard');
INSERT INTO pret(date, duree, adherent)
	VALUES ('10-10-2020', 21, 'lbernard');

/*-------------------- TABLE EXEMPLAIRE-PRET DATASET IMPORTS ----------------------*/
INSERT INTO exemplairepret(
	pret, ref)
VALUES (1, 10057);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (1, 1125);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (1, 5509);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (2, 8966);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (2, 9000);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (3, 5506);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (3, 5507);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (4, 10056);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (4, 5510);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (5, 6000);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (5, 623);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (6, 11456);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (6, 5236);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (7, 5696);
INSERT INTO exemplairepret(
	pret, ref)
VALUES (7, 8965);

/*------------------------------ CONTRIBUTEURS TABLE IMPORTS -----------------------------------*/
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (1,'Justin', 'Bridou', '22/05/1970', 'Française');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (2,'Jean', 'Marc', '03/09/1985', 'Allemand');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (3,'George', 'Lucas', '15/07/1966', 'Américaine');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (4,'Hayao', 'Miyazaki', '20/07/1950', 'Japonaise');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (5,'Walt', 'Disney', '22/05/1970', 'américaine');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (6,'Paul', 'Leroux', '12/07/1978', 'Française');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (7,'Jule', 'Julien', '13/11/1980', 'Chinois');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (8,'Pierre', 'Paul-Jacque', '23/04/1977', 'Brésilien');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (9,'Jeanne', 'Patrick', '14/07/1987', 'Française');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (10,'Lucy', 'Panier', '17/08/1975', 'Anglaise');

INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (11,'Donald', 'Duck', '2/07/1945', 'Canadien');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (12,'Mickey', 'moose', '01/10/1940', 'américaine');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (13,'Solan', 'Matthieu', '22/10/1999', 'Française');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (14,'Nicolas', 'Dimitri', '22/05/2000', 'Chinois');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (15,'Marchal', 'Marine', '22/03/2000', 'Allemande');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (16,'Marius', 'Brisson', '2/06/1980', 'Française');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (17,'Benjamin', 'Persi', '01/07/1980', 'Espagnol');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (18,'Jacques', 'Margaillan', '12/10/1975', 'Italien');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (19,'Clara', 'Jaune', '02/11/1975', 'Anglaise');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (20,'Fiona', 'Brown', '22/07/1995', 'Américaine');

INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (21,'Mathias', 'Pappatico', '2/08/2000', 'Italien');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (22,'Effertz', 'Lena', '10/04/1985', 'Américaine');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (23,'Dupret', 'Jeanne', '15/07/1995', 'Espagnol');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (24,'Yi', 'Nicolas', '04/10/1996', 'Canadien');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (25,'Carmona', 'Jean-Phillipe', '14/05/1980', 'Américaine');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (26,'Louise', 'Gillet', '24/09/2000', 'Népal');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (27,'Robiez', 'Calvin', '17/10/1987', 'Japonais');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (28,'Oliver', 'Lisa', '10/05/1978', 'Française');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (29,'Cadiou', 'Sébastien', '25/04/1978', 'Belge');
INSERT INTO contributeur(
	contributeurid, nom, prenom, datenaissance, nationalite)
	VALUES (30,'Baudry', 'Reem', '17/02/1998', 'Japonais');

/*--------------------------------------- INTERPRETES TABLE IMPORTS ----------------------------------*/

INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (6666, 1);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (4365, 7);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (9133, 8);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (222, 9);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (127, 16);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (731, 17);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (134, 18);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (1111, 19);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (7862, 25);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (8254, 26);
INSERT INTO interprete(
	oeuvremusicale, interprete)
	VALUES (2345, 27);

/*----------------------------------------------- COMPOSITEURS TABLE IMPORTS ------------------------------------*/
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (6666, 10);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (4365, 11);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (9133, 12);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (222, 13);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (127, 14);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (731, 15);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (134, 2);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (1111, 3);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (7862, 4);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (8254, 5);
INSERT INTO compositeur(
	oeuvremusicale, compositeur)
	VALUES (2345, 6);

/*--------------------------------------- AUTEURS TABLE IMPORTS ----------------------------*/
INSERT INTO auteur(
	livre, auteur)
	VALUES (4562, 21);
INSERT INTO auteur(
	livre, auteur)
	VALUES (5325, 22);
INSERT INTO auteur(
	livre, auteur)
	VALUES (4873, 23);
INSERT INTO auteur(
	livre, auteur)
	VALUES (4123, 24);
INSERT INTO auteur(
	livre, auteur)
	VALUES (4942, 25);
INSERT INTO auteur(
	livre, auteur)
	VALUES (1713, 26);
INSERT INTO auteur(
	livre, auteur)
	VALUES (7623, 27);
INSERT INTO auteur(
	livre, auteur)
	VALUES (9826, 28);
INSERT INTO auteur(
	livre, auteur)
	VALUES (7453, 29);
INSERT INTO auteur(
	livre, auteur)
	VALUES (1752, 30);

/*----------------------------------- ACTEURS TABLE IMPORTS -------------------------------------*/

INSERT INTO acteur(
	film, acteur)
	VALUES (12345, 11);
INSERT INTO acteur(
	film, acteur)
	VALUES (3495, 12);
INSERT INTO acteur(
	film, acteur)
	VALUES (3496, 13);
INSERT INTO acteur(
	film, acteur)
	VALUES (8921, 14);
    INSERT INTO acteur(
	film, acteur)
	VALUES (7896, 15);
INSERT INTO acteur(
	film, acteur)
	VALUES (8791, 16);
INSERT INTO acteur(
	film, acteur)
	VALUES (437, 17);
INSERT INTO acteur(
	film, acteur)
	VALUES (98756, 18);
INSERT INTO acteur(
	film, acteur)
	VALUES (888, 19);
INSERT INTO acteur(
	film, acteur)
	VALUES (5625, 20);

/*------------------------------------ REALISATEURS TABLE IMPORTS ------------------------------*/

INSERT INTO realisateur(
	film, realisateur)
	VALUES (12345, 1);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (3495, 2);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (3496, 3);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (8921, 4);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (7896, 5);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (8791, 6);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (437, 7);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (98756, 8);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (888, 9);
INSERT INTO realisateur(
	film, realisateur)
	VALUES (5625, 10);

