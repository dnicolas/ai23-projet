CREATE OR REPLACE VIEW Exemplaires_Disponibles AS
    SELECT DISTINCT(codeunique), titre, editeur, genre 
    FROM ressource 
    JOIN exemplaire ON exemplaire.ressource = ressource.codeunique 
    WHERE disponible = TRUE;
CREATE OR REPLACE VIEW Repartition_Emprunts AS
    SELECT
        CASE 
            WHEN exemplaire.ressource IN (SELECT codeunique FROM livre) THEN 'livre'
            WHEN exemplaire.ressource IN (SELECT codeunique FROM film) THEN 'film'
            WHEN exemplaire.ressource IN (SELECT codeunique FROM oeuvremusicale) THEN 'oeuvre musicale'
        END AS "category", COUNT(exemplaire.ressource) AS "nb_ressources", (COUNT(exemplaire.ressource)/SUM(COUNT(exemplaire.ressource)) OVER()) * 100 AS "part"
    FROM exemplaire
    JOIN exemplairepret ON exemplaire.ref = exemplairepret.ref
    GROUP BY "category";

CREATE OR REPLACE VIEW VLivre AS
    SELECT r.codeunique codeunique, r.titre titre, r.dateapparition dateapparition, r.editeur editeur, r.genre genre, r.codeclassification codeclassification, l.isbn isbn, l.resume resume, l.langue langue
    FROM Ressource r JOIN Livre l ON l.codeUnique = r.codeUnique;

CREATE OR REPLACE VIEW VOeuvreMusicale AS
    SELECT r.codeunique codeunique, r.titre titre, r.dateapparition dateapparition, r.editeur editeur, r.genre genre, r.codeclassification codeclassification, om.longueur longueur
    FROM OeuvreMusicale om JOIN Ressource r ON om.codeUnique = r.codeUnique;

CREATE OR REPLACE VIEW VFilm AS
    SELECT r.codeunique codeunique, r.titre titre, r.dateapparition dateapparition, r.editeur editeur, r.genre genre, r.codeclassification codeclassification, f.longueur longueur, f.synopsis synopsis
    FROM Film f JOIN Ressource r ON f.codeUnique = r.codeUnique;

CREATE OR REPLACE VIEW VAdherent AS
    SELECT u.login login, u.motdepasse motdepasse, u.nom nom, u.prenom prenom, u.adresse adresse, u.mail mail, a.numcarte numcarte, a.datenaissance datenaissance, a.numtel numtel, a.dateadhesion dateadhesion, a.blacklisted blacklisted
    FROM Utilisateur u JOIN Adherent a ON u.login = a.login;

CREATE OR REPLACE VIEW VPersonnel AS
    SELECT u.login login, u.motdepasse motdepasse, u.nom nom, u.prenom prenom, u.adresse adresse, u.mail mail
    FROM Utilisateur u JOIN Personnel p ON u.login = p.login;
    
CREATE OR REPLACE VIEW RessourcesPopulaires AS
    SELECT r.titre titre, r.codeUnique codeUnique, COUNT(ep.ref) nb_emprunt 
    FROM Ressource r JOIN Exemplaire e ON e.ressource = r.codeUnique JOIN ExemplairePret ep ON ep.ref = e.ref 
    GROUP BY r.titre, r.codeUnique 
    HAVING COUNT(ep.ref) > 2 
    ORDER BY COUNT(ep.ref) DESC;

CREATE OR REPLACE VIEW GenresPopulaires AS
    SELECT r.genre genre, COUNT(ep.ref) nb_emprunt 
    FROM Ressource r JOIN Exemplaire e ON e.ressource = r.codeUnique JOIN ExemplairePret ep ON ep.ref = e.ref 
    GROUP BY r.genre 
    HAVING COUNT(ep.ref) > 2 
    ORDER BY COUNT(ep.ref) DESC;

CREATE OR REPLACE VIEW LivresPopulaire AS
    SELECT vl.titre titre, vl.codeUnique codeUnique, COUNT(ep.ref) nb_emprunt
    FROM VLivre vl JOIN Exemplaire e ON e.ressource = vl.codeUnique JOIN ExemplairePret ep ON ep.ref = e.ref
    GROUP BY vl.titre, vl.codeUnique 
    HAVING COUNT(ep.ref) > 2 
    ORDER BY COUNT(ep.ref) DESC;

CREATE OR REPLACE VIEW OeuvresMusicalesPopulaire AS
    SELECT vom.titre titre, vom.codeUnique codeUnique, COUNT(ep.ref) nb_emprunt
    FROM VOeuvreMusicale vom JOIN Exemplaire e ON e.ressource = vom.codeUnique JOIN ExemplairePret ep ON ep.ref = e.ref   
    GROUP BY vom.titre, vom.codeUnique 
    HAVING COUNT(ep.ref) > 2 
    ORDER BY COUNT(ep.ref) DESC;

CREATE OR REPLACE VIEW FilmsPopulaires AS 
    SELECT vf.titre titre, vf.codeUnique codeUnique, COUNT(ep.ref) nb_emprunt
    FROM VFilm vf JOIN Exemplaire e ON e.ressource = vf.codeUnique JOIN ExemplairePret ep ON ep.ref = e.ref
    GROUP BY vf.titre, vf.codeUnique 
    HAVING COUNT(ep.ref) > 2 
    ORDER BY COUNT(ep.ref) DESC;

CREATE OR REPLACE VIEW DureeMaxEmpruntParAdherent AS
    SELECT va.nom, va.login login, MAX(p.duree) dureeEmpruntMax
    FROM VAdherent va JOIN Pret p ON p.adherent = va.login 
    GROUP BY va.nom, va.login
    ORDER BY MAX(p.duree) DESC;

CREATE OR REPLACE VIEW DureeMoyenneEmpruntParAdherent AS
    SELECT va.nom nom, va.login login, AVG(p.duree) dureeMoyenneEmprunt
    FROM VAdherent va JOIN Pret p ON p.adherent = va.login 
    GROUP BY va.nom, va.login
    ORDER BY AVG(p.duree) DESC;

CREATE OR REPLACE VIEW DureeMaxEmpruntParRessource AS
    SELECT r.titre, r.codeUnique codeUnique, MAX(p.duree) dureeEmpruntMax
    FROM Ressource r JOIN Exemplaire e ON e.ressource = r.codeUnique JOIN ExemplairePret ep ON ep.ref = e.ref JOIN Pret p ON p.pretID = ep.pret
    GROUP BY r.titre, r.codeUnique
    ORDER BY MAX(p.duree) DESC;

CREATE OR REPLACE VIEW DureeMoyenneEmpruntParRessource AS
    SELECT r.titre, r.codeUnique codeUnique, AVG(p.duree) dureeMoyenneEmprunt
    FROM Ressource r JOIN Exemplaire e ON e.ressource = r.codeUnique JOIN ExemplairePret ep ON ep.ref = e.ref JOIN Pret p ON p.pretID = ep.pret
    GROUP BY r.titre, r.codeUnique
    ORDER BY AVG(p.duree) DESC;


