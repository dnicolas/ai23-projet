from typing import Dict, Tuple

import psycopg2
from DataMapper import DataMapper

class RessourceDataMapper(DataMapper):

    def getAvailableExemplaires(self):
        connection = self.connect()
        try:
            cursor: psycopg2.extensions.cursor = connection.cursor()
            cursor.execute("SELECT * FROM Exemplaires_Disponibles")
            return cursor.fetchall()
        except Exception as err:
            print(err)
        finally:
            connection.close()

    def getRepartitionEmprunts(self):
        connection = self.connect()
        try:
            cursor: psycopg2.extensions.cursor = connection.cursor()
            cursor.execute("SELECT * FROM Repartition_Emprunts")
            return cursor.fetchall()
        except Exception as err:
            print(err)
        finally:
            connection.close()
            
    def ajouterRessource(self, codeunique, titre, dateapparition, editeur, genre, codeclassification) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "INSERT INTO ressource(codeunique, titre, dateapparition, editeur, genre, codeclassification) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', {5})".format(codeunique, titre, dateapparition, editeur, genre, codeclassification)
            cursor.execute(sql1)
            conn.commit()
        except Exception as err:
            print(err)
        finally:
            conn.close()

    def ajouterLivre(self, codeunique, titre, dateapparition, editeur, genre, codeclassification, isbn, resume, langue) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            self.ajouterRessource(self, codeunique, titre, dateapparition, editeur, genre, codeclassification)
            sql1 = "INSERT INTO livre(codeunique, isbn, resume, langue) VALUES ({0}, '{1}', '{2}','{3}')".format(codeunique, isbn, resume, langue)
            cursor.execute(sql1)
            conn.commit()
        except Exception as err:
            print(err)
        finally:
            conn.close()

    def ajouterOeuvreMusicale(self, codeunique, titre, dateapparition, editeur, genre, codeclassification, longueur) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            self.ajouterRessource(self, codeunique, titre, dateapparition, editeur, genre, codeclassification)
            sql1 = "INSERT INTO oeuvremusicale(codeunique, longueur) VALUES ({0}, {1})".format(codeunique, longueur)
            cursor.execute(sql1)
            conn.commit()
        except Exception as err:
            print(err)
        finally:
            conn.close()

    def ajouterFilm(self, codeunique, titre, dateapparition, editeur, genre, codeclassification, longueur, synopsis) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            self.ajouterRessource(self, codeunique, titre, dateapparition, editeur, genre, codeclassification)
            sql1 = "INSERT INTO film(codeunique, longueur, synopsis) VALUES ({0}, {1}, '{2}')".format(codeunique, longueur, synopsis)
            cursor.execute(sql1)
            conn.commit()
        except Exception as err:
            print(err)
        finally:
            conn.close()

