from datetime import date
from enum import Enum

import psycopg2
from DataMapper import DataMapper

class SanctionDataMapper(DataMapper):

    def blacklister(self, login):
        connection = self.connect()
        try:
         cursor : psycopg2.extensions.cursor = connection.cursor()
         cursor.execute(f"UPDATE adherent SET blacklisted = TRUE WHERE login = '{login}'")
         connection.commit()
        except Exception as err:
            print(err)
        finally:
            connection.close()
    def getSanctions(self):
        connection = self.connect()
        try:
         cursor : psycopg2.extensions.cursor = connection.cursor()
         cursor.execute(f"SELECT sanctionid, adherent, datesanction, encours FROM sanction")
         return cursor.fetchall()
        except Exception as err:
            print(err)
        finally:
            connection.close()
dataMapper = SanctionDataMapper()