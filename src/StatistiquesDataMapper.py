import psycopg2
import string
from DataMapper import *

class StatistiquesDataMapper(DataMapper):
    # affiche la liste des exemplaires les plus emprunté dans l'ordre décroissant
    def ressourcesPopulaires(self) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT rp.titre, rp.nb_emprunt FROM RessourcesPopulaires rp"
            cursor.execute(sql1)
            rows = cursor.fetchall()
            return rows
        except Exception as err:
            print(err)
        finally:
            conn.close()

        # print('Titre : {0} Nombre d\'emprunts : {1}\n'.format(row[0], row[1]))

    # affiche le nombre de fois qu'une ressource en particulier a été empruntée
    def nombreEmpruntRessource(self, codeUnique : int) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT rp.titre, rp.nb_emprunt FROM RessourcesPopulaires rp WHERE rp.codeUnique = '{0}'".format(codeUnique)
            cursor.execute(sql1)
            ressource = cursor.fetchone()
            return ressource[0]
        except Exception as err:
            print(err)
        finally:
            conn.close()
        #print("Titre : {0} Nombre d\'emprunts : {1}".format(ressource[0], ressource[1]))

    # nombre de ressources empruntés par genre
    def genresPopulaires(self) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT gp.genre, gp.nb_emprunt FROM GenresPopulaires gp"
            cursor.execute(sql1)
            rows = cursor.fetchall()
            return rows
        except Exception as err:
            print(err)
        finally:
            conn.close()
        #  print('Genre : {0} Nombre d\'emprunts : {1}\n'.format(row[0], row[1]))

    # retourne le nombre de fois qu'un genre a été empruntée
    def nombreEmpruntParGenre(self, genre : int) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT gp.genre, gp.nb_emprunt FROM GenresPopulaires gp WHERE gp.genre = '{0}' GROUP BY  gp.genre".format(genre)
            cursor.execute(sql1)
            ressource = cursor.fetchone()
            return ressource[1]
        except Exception as err:
            print(err)
        finally:
            conn.close()
        #print("Genre : {0} Nombre d\'emprunts : {1}".format(ressource[0], ressource[1]))

    # retourne le genre de docuement le plus emprunté par un adhérent pour suggestion 
    def genrePrefereAdherent(self, login : string) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT gppa.genre, gppa.nb_emprunt FROM GenrePrefereParAdherent gppa WHERE gppa.login = '{0}'".format(login)
            cursor.execute(sql1)
            genre = cursor.fetchall()
            return genre
        except Exception as err:
            print(err)
        finally:
            conn.close()
        #print("Genre le plus emprunté : {0}".format(genre[1]))

    #retourne l'age moyen des adhérents
    def ageMoyenAdherent(self) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT AVG(DATE_PART('year', now()) - DATE_PART('year', a.dateNaissance)) FROM Adherent a"
            cursor.execute(sql1)
            age = cursor.fetchone()
            return age[0]
        except Exception as err:
            print(err)
        finally:
            conn.close()
        #print("Genre le plus emprunté : {0}".format(genre[1]))

    def typeDocumentPopulaire(self) : 
        # retourne le type de document le plus emprunté ( livre, oeuvre musicale ou film )
        # on récupère le nombre de livre emprunté, le nb de oeuvre musicale empruntées et le nb de films empruntés.
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT lp.nb_emprunt FROM LivresPopulaire lp"
            cursor.execute(sql1)
            nbLivres = cursor.fetchone()
            sql2 = "SELECT omp.nb_emprunt FROM OeuvresMusicalesPopulaire omp" 
            cursor.execute(sql2)
            nbOeuvres = cursor.fetchone()
            sql3 = "SELECT fp.nb_emprunt FROM FilmsPopulaires fp" 
            cursor.execute(sql3)
            nbFilms = cursor.fetchone()
            if(nbLivres[0] > nbFilms[0] and nbLivres[0] > nbOeuvres[0]) : 
                return "livres"
            elif(nbFilms[0] >= nbLivres[0] and nbFilms[0] > nbOeuvres[0]) : 
                return "oeuvres musicales"
            elif(nbOeuvres[0] > nbFilms[0] and nbOeuvres[0] > nbLivres[0]) : 
                return "films"
        except Exception as err:
            print(err)
        finally:
            conn.close()

    def dureeMoyenneEmpruntParAdherent(self, login) : 
        #DureeMaxEmpruntParAdherent
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT dmepa.dureeMoyenneEmprunt FROM DureeMoyenneEmpruntParAdherent dmepa WHERE dmepa.login = '{0}'".format(login)
            cursor.execute(sql1)
            duree = cursor.fetchone()
            return duree[0]
        except Exception as err:
            print(err)
        finally:
            conn.close()

    def dureeMoyenneEmprunt(self) : 
        #DureeMaxEmpruntParAdherent
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT AVG(duree) FROM Pret ORDER BY AVG(duree) DESC"
            cursor.execute(sql1)
            duree = cursor.fetchone()
            return duree[0]
        except Exception as err:
            print(err)
        finally:
            conn.close()

    def dureeMaxEmpruntParAdherent(self, login) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT dmepa.dureeEmpruntMax FROM DureeMaxEmpruntParAdherent dmepa WHERE dmepa.login = '{0}'".format(login)
            cursor.execute(sql1)
            duree = cursor.fetchone()
            return duree[0]
        except Exception as err:
            print(err)
        finally:
            conn.close()
    
    def dureeMaxPret(self) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT MAX(duree) FROM Pret ORDER BY MAX(duree) DESC"
            cursor.execute(sql1)
            durees = cursor.fetchall()
            return durees
        except Exception as err:
            print(err)
        finally:
            conn.close()

    def dureeMoyenneEmpruntParRessource(self, codeUnique) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT dmepr.dureeMoyenneEmprunt FROM DureeMoyenneEmpruntParRessource dmepr WHERE dmepr.codeUnique = '{0}'".format(codeUnique)
            cursor.execute(sql1)
            duree = cursor.fetchone()
            return duree[0]
        except Exception as err:
            print(err)
        finally:
            conn.close()
    
    def dureeMaxEmpruntParRessource(self, codeUnique) : 
        conn = self.connect()
        try:
            cursor : psycopg2.extensions.cursor = conn.cursor()
            sql1 = "SELECT dmepr.dureeEmpruntMax FROM DureeMaxEmpruntParRessource dmepr WHERE dmepr.codeUnique = '{0}'".format(codeUnique)
            cursor.execute(sql1)
            duree = cursor.fetchone()
            return duree[0]
        except Exception as err:
            print(err)
        finally:
            conn.close()
