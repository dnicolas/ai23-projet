from DataMapper import *


class AdherentDataMapper(DataMapper):

    def selectAdherents(self):
        connection = self.connect()
        try:
         cursor : psycopg2.extensions.cursor = connection.cursor()
         cursor.execute("SELECT * FROM public.adherent")
         data = cursor.fetchall()
         print(data)
        except Exception as err:
            print(err)
        finally:
            connection.close()


    def getPrets(self, login):
        connection = self.connect()
        try:
         cursor : psycopg2.extensions.cursor = connection.cursor()
         cursor.execute(f"SELECT COUNT(exemplairepret.pret) AS nb_articles,pret.pretid, pret.date, pret.adherent "
                        f"FROM pret JOIN exemplairepret ON pret.pretid = exemplairepret.pret "
                        f"WHERE pret.adherent = '{login}' GROUP BY pret.pretid")
         return cursor.fetchall()
        except Exception as err:
            print(err)
        finally:
            connection.close()

datamapper = AdherentDataMapper()