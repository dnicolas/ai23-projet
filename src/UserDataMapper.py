from datetime import date
from enum import Enum

import psycopg2
from DataMapper import DataMapper

class Roles(Enum):
    UNAUTHORIZED = -1
    BLACKLISTED = -2
    ADHERENT = 1
    PERSONNEL = 2
class UserDataMapper(DataMapper):
    def ajouterUtilisateur(self, login:str, modtdepasse:str, nom:str, prenom:str, adresse:str, mail:str):
        connection = self.connect()
        try:
            cursor: psycopg2.extensions.cursor = connection.cursor()
            cursor.execute(f"INSERT INTO utilisateur(login, motdepasse, nom, prenom, adresse, mail)"
                           f"VALUES ('{login}', '{modtdepasse}', '{nom}', '{prenom}','{adresse}','{mail}', CURRENT_TIMESTAMP, FALSE)")
            connection.commit()
        except Exception as err:
            print(err)
        finally:
            connection.close()
    def ajouterAdherent(self, login : str, numcarte: str, datenaissance:date, numtel:str):
        connection = self.connect()
        try:
            cursor: psycopg2.extensions.cursor = connection.cursor()
            cursor.execute(f"INSERT INTO adherent(login, numcarte, datenaissance, numtel, dateadhesion, blacklisted)"
                           f"VALUES ('{login}', '{numcarte}', '{datenaissance}', '{numtel}', CURRENT_TIMESTAMP, FALSE)")
            connection.commit()
        except Exception as err:
            print(err)
        finally:
            connection.close()
    def deleteAdherent(self, login:str):
        connection = self.connect()
        try:
            cursor: psycopg2.extensions.cursor = connection.cursor()
            cursor.execute(f"DELETE FROM adherent WHERE login = '{login}'")
            connection.commit()
        except Exception as err:
            print(err)
        finally:
            connection.close()
    def ajouterPersonnel(self, login: str):
        connection = self.connect()
        try:
            cursor: psycopg2.extensions.cursor = connection.cursor()
            cursor.execute(f"INSERT INTO personnel(login) VALUES ('{login}')")
            connection.commit()
        except Exception as err:
            print(err)
        finally:
            connection.close()
    def deletePersonnel(self, login:str):
        connection = self.connect()
        try:
            cursor: psycopg2.extensions.cursor = connection.cursor()
            cursor.execute(f"DELETE FROM personnel WHERE login = '{login}'")
            connection.commit()
        except Exception as err:
            print(err)
        finally:
            connection.close()
    def checkUser(self, login : str, password: str) -> Roles:
        connection = self.connect()
        try:
            cursor: psycopg2.extensions.cursor = connection.cursor()
            cursor.execute(f"SELECT * FROM utilisateur WHERE login = '{login}' AND motDePasse = '{password}'")
            data = cursor.fetchone()
            print(data)
            if data is None:
                return Roles.UNAUTHORIZED
            cursor.execute(f"SELECT * FROM personnel WHERE login = '{login}'")
            data = cursor.fetchone()
            if data is not None:
                return Roles.PERSONNEL
            cursor.execute(f"SELECT * FROM adherent WHERE login = '{login}'")
            data = cursor.fetchone()
            if data is not None:
                if data[5]: # If Blacklisted
                    return Roles.BLACKLISTED
                return Roles.ADHERENT
        except Exception as err:
            print(err)
        finally:
            connection.close()


dataMapper = UserDataMapper()
dataMapper.deleteAdherent("blussier")
dataMapper.deletePersonnel("hlefebvre")
print(dataMapper.checkUser("hlefebvre", "1888ZFDSDSQD5Q"))